

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LogoutServlet
 */
@WebServlet("/LogoutServlet")
public class LogoutServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		 Cookie cookie = null;
	        Cookie[] cookies = null;
	        
		PrintWriter out=res.getWriter();
     res.setContentType("text/html");        
     String aname=req.getParameter("aname");
     cookies=req.getCookies();
     for (int i = 0; i < cookies.length; i++) {
         cookie = cookies[i];
     
     if((cookie.getName().compareTo("aname")==0)&&(cookie.getValue().compareTo(aname)==0)) {
         cookie.setMaxAge(0);
         cookie.setValue(null);
     }
     }
     HttpSession session=req.getSession();
     if(session.getAttribute("aname")==aname) {
     	session.setAttribute("ename", null);
     }
out.println("<p style='color:green;font-size:30px'>Admin logout successfully....</p>");
     	RequestDispatcher rd=req.getRequestDispatcher("admin.html");
     	rd.include(req, res);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
