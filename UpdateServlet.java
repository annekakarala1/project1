

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		PrintWriter out=res.getWriter();
        res.setContentType("text/html");        
        String eno=req.getParameter("eno");
        String ename=req.getParameter("ename");
        String pwd=req.getParameter("pwd");
        String email=req.getParameter("em");
        int rm=Integer.parseInt(req.getParameter("rm"));
        out.println(eno+" "+ename+" "+pwd+" "+email+" "+rm);
        
        try
        {
        	Connection con=JDBC.getConnection();
        	PreparedStatement pst=con.prepareStatement("update emp set ename=?,pwd=?,email=?,ramount=? where eno=?");
        	pst.setString(1, ename);
        	pst.setString(2, pwd);
        	pst.setString(3, email);
        	pst.setInt(4,rm );
        	pst.setInt(5, Integer.parseInt(eno));
        	
        	int c=pst.executeUpdate();
        	if(c==1) {
        	RequestDispatcher rd=req.getRequestDispatcher("addrem");
        	rd.forward(req, res);}
        	out.println(".......other case");
        }catch(Exception e) {
        	e.printStackTrace();
        }
	
	}

}
