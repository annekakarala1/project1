

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AddRemb
 */
@WebServlet("/AddRemb")
public class AddRemb extends HttpServlet {
	
	
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		PrintWriter out=res.getWriter();
        res.setContentType("text/html");        
        int eno=Integer.parseInt(req.getParameter("eno"));
        String rmt=req.getParameter("rmt");

        try
        {
        	Connection con=JDBC.getConnection();
        	PreparedStatement pst=con.prepareStatement("insert into remb values(?,?,?)");
        	pst.setInt(1, eno);
        	pst.setInt(2, Integer.parseInt(rmt));
        	pst.setString(3, "n");
        	int ct=pst.executeUpdate();
        	if(ct==1) {
        		out.println("<p style='color:green;font-size:20px;'>reimbursement of employee added successfully....</p>");
                
            	RequestDispatcher rd=req.getRequestDispatcher("login");
            	rd.include(req, res);
        	}else {
out.println("<p style='color:red;font-size:20px'>reimbursement of employee not added....</p>");
                
            	RequestDispatcher rd=req.getRequestDispatcher("login");
            	rd.include(req, res);
        	}
        }catch(Exception e) {
        	e.printStackTrace();
        }
	}

}
