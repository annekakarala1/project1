

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class EditServet
 */
@WebServlet("/EditServet")
public class EditServet extends HttpServlet {
	
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		PrintWriter out=res.getWriter();
        res.setContentType("text/html");        
        int eno=Integer.parseInt(req.getParameter("eno"));
        try
        {
        	Connection con=JDBC.getConnection();
        	PreparedStatement pst=con.prepareStatement("select * from emp where eno="+eno);
        	ResultSet rs=pst.executeQuery();
        	out.print("<!doctype html><html><head><title>Edit Application</title><style>#editing{width:500px;padding:20px;float:left;background-color:lemonchiffon;}table thead th{background-color:#efd706;}label{float:left;width:200px}input{float:right;width:200px;}h1{color:orange;}</style></head><h1>Edit Employee Details<h1><br/> <br/>");  
	        out.println("<center><div id='editing'><form method='post' action='update?eno="+eno+"'>");
        	while(rs.next()) {
                 String fg=rs.getString(6);
        		if(fg.equals("n"))
    	         out.print("<label>Eno:</label><input type='text' id='eno' name='eno' value='"+rs.getInt(1)+"' disabled/><br/><br/><label>Ename:</label><input type='text' id='ename' name='ename' value='"+rs.getString(2)+"' required/><br/><label>Password:</label><input type='password' name='pwd' id='pwd' value='"+rs.getString(3)+"' required/><br/><br/><label>Email:</label><input type='email' name='em' id='em' value='"+rs.getString(4)+"' required/><br/><br/><label>Reimbursement Amount:</label><input type=number id='rm' name='rm' value='"+rs.getInt(5)+"' required/><br/><br/><label>Reimbursement Flag :</label><input type='button' value='Pending' disabled/>");  
    	         else if(fg.equals("y"))
    	        	 out.print("<label>Eno:</label><input type='text' id='eno' name='eno' value='"+rs.getInt(1)+"' disabled/><br/><label>Ename:</label><input type='text' id='ename' name='ename' value='"+rs.getString(2)+"' required/><br/><br/><label>Password:</label><input type='password' name='pwd' id='pwd' value='"+rs.getString(3)+"' required/><br/><br/><label>Email:<input type='email' name='em' id='em' value='"+rs.getString(4)+"' required/><br/><br/><label>Reimbursement Amount:</label><input type=number id='rm' name='rm' value='"+rs.getInt(5)+"' required/><br/><br/><label>Reimbursement Flag :</label><input type='button' value='Approved' disabled/>");  
   	         	 
        	}  
    	        out.print("<br/><br/><input type='submit' name='sub' id='sub' value='Update' style='width:300px;background-color:cadetblue;border:2px solid gray;height:40px;border-radius:20px'/></form></div></center></body></html>"); 
        	
        }
        catch (Exception e){
            e.printStackTrace();
        }
	}

	

}
