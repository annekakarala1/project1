

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class AdminServlet
 */
@WebServlet("/AdminServlet")
public class AdminServlet extends HttpServlet {
		protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
			 PrintWriter out=res.getWriter();
		        res.setContentType("text/html");        
		        String aname=req.getParameter("aname"); 
		        String apwd=req.getParameter("apwd");

		        try
		        {
		        	Connection con=JDBC.getConnection();
		        	PreparedStatement pst=con.prepareStatement("select * from manager where mname=? and mpwd=?");
		        	pst.setString(1, aname);
		        	pst.setString(2, apwd);
		        	ResultSet rss=pst.executeQuery();
		        	rss.next();
		        	out.println("<div id='div1'><a href='logout?aname="+rss.getString(2)+"'>Logout</a></div>");
	                
		        	Statement stmt = con.createStatement();
		            //Retrieving the data
		            ResultSet rs1 = stmt.executeQuery("select count(*) from manager where mname='"+aname+"' and mpwd='"+apwd+"'");
		            rs1.next();
		            int count=rs1.getInt("count(*)");
		        	
		            //session creation
		            if(count==1) {
		            HttpSession session=req.getSession();  
		            session.setAttribute("aname",aname); 
		           //cookie creation
		            Cookie ck=new Cookie("aname",aname);
		            //creating cookie object  
		            res.addCookie(ck);//adding cookie in the response  
		           
		            out.println("<!doctype html>");
		            out.println("<html><head><title>Admin DashBoard</title>");
		            out.println("<style>#div1{float:right;width:100px;}table{width:700px;background-color:lemonchiffon;border:2px solid gray;border-radius:10px;}th{background-color:#efd706 !important;}h1{color:orange;}#addRem{width:400px !important;height:300px;background-color:lemonchiffon;}</style>");
		            out.println("<script src='https://code.jquery.com/jquery-3.5.1.min.js'></script><script> $(document).ready(function(){$('#addform').hide();$('#addrem').click(function(){$('#addform').toggle(3000);});});</script></head><body>");
		            out.print("<center><h1>Admin DashBoard</h1><br/><table border='1' width='100%'");  
			        out.print("<tr><th>Eno</th><th>RAmount</th><th>RFlag</th><th>Edit</th></tr>");  
			     
			        PreparedStatement pst1=con.prepareStatement("select * from remb");
		        	ResultSet rs=pst1.executeQuery();
		        	while(rs.next()) {
		        		out.println("<form method='get' action='ef?eno='"+rs.getInt(1)+">");
			        	
		        		out.println("<tr><td>"+rs.getInt(1)+"</td><td>"+rs.getInt(2)+"</td><td><input type='radio' value='n' />Pending<input type='radio' value='y' name='fg' checked/>Approved</td><td><a href='ef?eno='"+rs.getInt(1)+">Edit</a></td></tr>");  
		   		     
		        		
		        		
		        		}
		    	        out.println("<input type='submit' value='update'/></form></table>");
		    	        out.println("</center></body></html>");
		            }else {
		            	out.println("<p style='color:red'>Invalid admin details....</p>");
		            
		            	RequestDispatcher rd=req.getRequestDispatcher("admin.html");
		            	rd.include(req, res);
		            }
		        }
		        catch (Exception e){
		            e.printStackTrace();
		        }

}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
