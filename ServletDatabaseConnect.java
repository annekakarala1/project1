import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ServletDatabaseConnect extends HttpServlet  
{
    protected void doPost(HttpServletRequest req,HttpServletResponse res)throws ServletException,IOException
    {
        PrintWriter out=res.getWriter();
        res.setContentType("text/html");        
        String ename=req.getParameter("ename"); 
        String pwd=req.getParameter("pwd");

        try
        {
        	Connection con=JDBC.getConnection();
        	PreparedStatement pst=con.prepareStatement("select * from emp where ename=? and pwd=?");
        	pst.setString(1, ename);
        	pst.setString(2, pwd);
        	ResultSet rs=pst.executeQuery();
        	
        	Statement stmt = con.createStatement();
            //Retrieving the data
            ResultSet rs1 = stmt.executeQuery("select count(*) from emp where ename='"+ename+"' and pwd='"+pwd+"'");
            rs1.next();
            int count=rs1.getInt("count(*)");
        	//session creation
            if(count==1) {
            HttpSession session=req.getSession();  
            session.setAttribute("ename",ename); 
           //cookie creation
            Cookie ck=new Cookie("ename",ename);
            //creating cookie object  
            res.addCookie(ck);//adding cookie in the response  
           
            out.println("<!doctype html>");
            out.println("<html><head><title>Emp DashBoard</title>");
            out.println("<style>table{width:700px;background-color:lemonchiffon;border:2px solid gray;border-radius:10px;}th{background-color:#efd706 !important;}h1{color:orange;}#addRem{width:400px !important;height:300px;background-color:lemonchiffon;}#div1{float:right;width:100px;}</style>");
            out.println("<script src='https://code.jquery.com/jquery-3.5.1.min.js'></script><script> $(document).ready(function(){$('#addform').hide();$('#addrem').click(function(){$('#addform').toggle(3000);});});</script></head><body>");
            out.print("<center><h1>Employee DashBoard</h1><br/><table border='1' width='100%'");  
	        out.print("<tr><th>Eno</th><th>Ename</th><th>Pwd</th><th>Email</th><th>Ramount</th><th>RFlag</th><th>Edit</th></tr>");  
	       
        	while(rs.next()) {
        		
        		out.println("<div id='div1'><a href='dcs?ename="+rs.getString(2)+"'>Logout</a></div>");
                out.print("<tr><td>"+rs.getInt(1)+"</td><td>"+rs.getString(2)+"</td><td>"+rs.getString(3)+"</td><td>"+rs.getString(4)+"</td><td>"+rs.getInt(5)+"</td><td>"+rs.getString(6)+"</td><td><a href='es?eno="+rs.getInt(1)+"'>edit</a></td></td></tr>");  
        	
    	        out.println("</table><br/><input type='button' value='AddReimbursement' style='width:300px;background-color:cadetblue;border:2px solid gray;height:60px;border-radius:20px' id='addrem' name='addrem'/>");
    	        out.println("<div id='addform' style='border:2px solid brown;border-radius:20px;width:600px;height:300px;padding:20px;'><h1>Add Reimbursement form</h1><br/>");
    	        out.println("<form method='post' action='addrem?eno="+rs.getInt(1)+"'><label>Eno:</label><input type='text' id='eno' name='eno' value='"+rs.getInt(1)+"' disabled/><br/><br/><label>Reimbursement Amount:</lable><input type='number' min='0' id='rmt' name='rmt' required/><br/><br/>");
        	} out.println("<input type='submit' value='Add Reimbursement' style='width:300px;background-color:cadetblue;border:2px solid gray;height:40px;border-radius:20px'/>");
    	        out.println("</form></div></center></body></html>");
            }else {
            	out.println("<p style='color:red'>Invalid employee details....</p>");
            
            	RequestDispatcher rd=req.getRequestDispatcher("index.html");
            	rd.include(req, res);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }
}